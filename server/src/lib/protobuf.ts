/**
 * Function decode a buffer and return the json object
 * @param buffer - buffer
 */
export async function protobufDecode(buffer: number): Promise<any> {
  return new Promise( resolve => {
      var protobuf = require("protobufjs");
      protobuf.load(`${process.cwd()}/src/types/connection.proto`, function(err, root) {
      if (err)
        throw err;

      // Obtain a message type
      var ConnectionsMessage = root.lookupType("conn.ConnectionsMessage");
      let start = Date.now()
      var message = ConnectionsMessage.decode(buffer);
      let end = Date.now()
      resolve({data: message, dss: end - start})
    });
  })
}