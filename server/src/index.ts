import { protobufDecode} from './lib/protobuf'
const WebSocket = require('ws');
const wss = new WebSocket.Server({ 
  port: 8080
});
 
wss.on('connection', function connection(ws) {
  ws.on('message', async function incoming(message) {
    //console.log('received: %s', message);
    try {
      // Is JSON
      JSON.parse(message)
      let resp = {time: Date.now()}
      ws.send(JSON.stringify(resp));
    } catch (error) {
      let now = Date.now()
      let resp1 = await protobufDecode(message);
      let resp = {time: now, DSS: resp1.dss }
      ws.send(JSON.stringify(resp));
    }
  });
});