"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const protobuf_1 = require("./lib/protobuf");
const WebSocket = require('ws');
const wss = new WebSocket.Server({
    port: 8080
});
wss.on('connection', function connection(ws) {
    ws.on('message', function incoming(message) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                JSON.parse(message);
                let resp = { time: Date.now() };
                ws.send(JSON.stringify(resp));
            }
            catch (error) {
                let now = Date.now();
                let resp1 = yield protobuf_1.protobufDecode(message);
                let resp = { time: now, DSS: resp1.dss };
                ws.send(JSON.stringify(resp));
            }
        });
    });
});
//# sourceMappingURL=index.js.map