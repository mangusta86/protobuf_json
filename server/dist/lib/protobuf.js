"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
function protobufDecode(buffer) {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise(resolve => {
            var protobuf = require("protobufjs");
            protobuf.load(`${process.cwd()}/src/types/connection.proto`, function (err, root) {
                if (err)
                    throw err;
                var ConnectionsMessage = root.lookupType("conn.ConnectionsMessage");
                let start = Date.now();
                var message = ConnectionsMessage.decode(buffer);
                let end = Date.now();
                resolve({ data: message, dss: end - start });
            });
        });
    });
}
exports.protobufDecode = protobufDecode;
//# sourceMappingURL=protobuf.js.map