export interface IBenchmark {
	name: string
	//Json
	jStart?: number 
	jEnd?: number
	jDTT?: number
	jDTQ?: number
	jDTS?: string
	jDSS?: number

	//Protobuf
	pStart?: number
	pEnd?: number
	pDTT?: number
	pDTQ?: number
	pDTS?: string
	pDSS?: number
	
	//Statistics
	

}

export interface IRawConnection {
  ".id": string,
	"protocol": string,
	"src-address": string,
	"dst-address": string,
	"reply-src-address": string,
	"reply-dst-address": string,
	"timeout": string,
	"orig-packets": string
	"orig-bytes": string,
	"orig-fasttrack-packets": string,
	"orig-fasttrack-bytes": string,
	"repl-packets": string,
	"repl-bytes": string,
	"repl-fasttrack-packets": string,
	"repl-fasttrack-bytes": string,
	"orig-rate": string,
	"repl-rate": string,
	"expected": string,
	"seen-reply": string,
	"assured": string,
	"confirmed": string,
	"dying": string,
	"fasttrack": string,
	"srcnat": string,
	"dstnat": string
}

export interface IConnection {
  id: string,
	protocol: string,
	srcAddress: string,
	dstAddress: string,
	replySrcAddress: string,
	replyDstAddress: string,
	timeout: string,
	origPackets: number,
	origBytes: number,
	origFasttrackPackets: number,
	origFasttrackBytes: number,
	replPackets: number,
	replBytes: number,
	replFasttrackPackets: number,
	replFasttrackBytes: number,
	origRate: number,
	replRate: number,
	expected: boolean,
	seenReply: boolean,
	assured: boolean,
	confirmed: boolean,
	dying: boolean,
	fasttrack: boolean,
	srcnat: boolean,
	dstnat: boolean
}