
var ProgressBar = require('progress');
var WebSocket = require('ws');
const ws = new WebSocket('ws://localhost:8080');
const fs = require('fs')

import { sleep } from './lib/basic'
import { WsSendData } from './lib/websocket'
import { convertJSONToBinary } from './lib/protobuf'
// Types
import { IBenchmark } from './types'

var bar


function main () {
  let benchmark: Array<IBenchmark> = []
  let files = fs.readdirSync(`${process.cwd()}/data`)
  files = files.sort()
  const totalTests: number = files.length * 2; 
  bar = new ProgressBar('Progress [:bar] :percent :etas ', { total: totalTests });
  let resCounter: number = 0 ;
  let sendCounter: number = 0 ; 
  let start: number, end: number ;
  ws.on('open', async function open() {
    console.log("Connected to WS server!")
    let data ;
    for (const file of files) {
      data = fs.readFileSync(`${process.cwd()}/data/${file}`)
      //+------+
      //| JSON |
      //+------+
      let jsonData = JSON.parse(data)
      start = Date.now()
      JSON.stringify(jsonData)
      end   = Date.now() ; 
      benchmark.push({name: file, jStart: Date.now(), jDTQ: data.length, jDSS: end-start})
      await WsSendData(ws, data);
      await sleep(500);

      //+----------+
      //| Protobuf |
      //+----------+
      let binary = await convertJSONToBinary(JSON.parse(data))
      benchmark[sendCounter].pDTQ = Buffer.from(binary.toString()).length
      benchmark[sendCounter].pStart = Date.now()
      await WsSendData(ws, binary)
      sendCounter++;
      await sleep(500);
    }
  });


  ws.on('message', function incoming(message) {
    bar.tick();
    let pos = Math.floor(resCounter/2);
    
    if (resCounter % 2 === 0 ) {
      benchmark[pos].jEnd = Number(JSON.parse(message).time)
      benchmark[pos].jDTT = benchmark[pos].jEnd - benchmark[pos].jStart 
    } else {
      // protobuf
      benchmark[pos].pEnd = Number(JSON.parse(message).time)
      benchmark[pos].pDTT = benchmark[pos].pEnd - benchmark[pos].pStart
      benchmark[pos].pDSS = JSON.parse(message).DSS 
    }
    
    if(resCounter === totalTests -1) {
      // Remove useless data and calculate
      for (let i = 0; i < benchmark.length; i++) {
        const element = benchmark[i];
        // --- Calculate
        element['jDTS'] = (Math.floor(((element['jDTQ'] / (element['jDTT']===0 ? 1 : element['jDTT']) ) * 1000 ) / 10**6)).toString()+ "MB/s"
        element['pDTS'] = (Math.floor(((element['pDTQ'] / (element['pDTT']===0 ? 1 : element['pDTT']) ) * 1000 ) / 10**6)).toString()+ "MB/s"
        // --- Statistics
        // element['pDTS'] = 
        // --- Remove
        delete element['jStart']
        delete element['jEnd']
        delete element['pStart']
        delete element['pEnd']
      }
      
      

      // Show results
      console.table(benchmark)
      process.exit()
    }
    resCounter++;
  });
}

main()


