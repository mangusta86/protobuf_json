/**
 * Function transforms standard async ws.send to sync  
 * @param ws - WebSocket object
 * @param data - what i send over WebSocket
 */
export async function WsSendData(ws,data,binary=false) {
  return new Promise( resolve => 
    ws.send(data,resolve)
  )
}