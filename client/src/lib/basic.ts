/**
 * Function is similar with C/C++ sleep function
 * @param ms Miliseconds for sleeping
 */
export function sleep(ms){
  return new Promise(resolve=>{
      setTimeout(resolve,ms)
  })
}
