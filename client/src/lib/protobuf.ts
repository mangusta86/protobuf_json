import {IConnection, IRawConnection} from '../types'

/**
 * Convert the JSON object with connections info into a buffer
 * @param rawConntections - Conections data
 */
export async function convertJSONToBinary( rawConntections: Array<IRawConnection> ): Promise<number> {
  let buffer : number
  let connections: Array<IConnection> = []
  
  //+----------------------------------------------+
  //| Convert from rawConection list to Connection |
  //+----------------------------------------------+
  for (let i = 0; i < rawConntections.length; i++) {
    const connection: IRawConnection = rawConntections[i];
    connections.push({
      id: connection[".id"],
      protocol: connection['protocol'],
      srcAddress: connection["src-address"],
      dstAddress: connection["dst-address"],
      replySrcAddress: connection["reply-src-address"],
      replyDstAddress: connection["reply-dst-address"],
      timeout: connection['timeout'],
      origPackets: parseInt(connection["orig-packets"]),
      origBytes: parseInt(connection["orig-bytes"]),
      origFasttrackPackets: parseInt(connection["orig-fasttrack-packets"]),
      origFasttrackBytes: parseInt(connection["orig-fasttrack-bytes"]),
      replPackets: parseInt(connection["repl-packets"]),
      replBytes: parseInt(connection["repl-bytes"]),
      replFasttrackPackets: parseInt(connection["repl-fasttrack-packets"]),
      replFasttrackBytes: parseInt(connection["repl-fasttrack-bytes"]),
      origRate: parseInt(connection["orig-rate"]),
      replRate: parseInt(connection["repl-rate"]),
      expected: Boolean(),
      seenReply: Boolean(),
      assured: Boolean(),
      confirmed: Boolean(),
      dying: Boolean(),
      fasttrack: Boolean(),
      srcnat: Boolean(),
      dstnat: Boolean()
    })
  }
  // console.log(`I found ${connections.length} connection!`)

  //+--------------------------+
  //| Convert JSON to protobuf |
  //+--------------------------+  
  buffer = await protobufEncode(connections)

  return(buffer)
}

/**
 * Function convert the conection array into a buffer
 * @param connections - Array with connection information
 */
async function protobufEncode(connections: Array<IConnection>): Promise<any> {
  return new Promise( resolve => {
      var protobuf = require("protobufjs");
      protobuf.load(`${process.cwd()}/src/types/connection.proto`, function(err, root) {
      if (err) 
        throw err;

      // Obtain a message type
      var ConnectionsMessage = root.lookupType("conn.ConnectionsMessage");
  
      // Exemplary payload
      var payload = { connections: connections};
      
      // Verify the payload if necessary (i.e. when possibly incomplete or invalid)
      var errMsg = ConnectionsMessage.verify(payload);
      if (errMsg)
        throw Error(errMsg);
  
      // Create a new message
      var message = ConnectionsMessage.create(payload); // or use .fromObject if conversion is necessary
      
      // Encode a message to an Uint8Array (browser) or Buffer (node)
      var buffer = ConnectionsMessage.encode(message).finish();
      resolve(buffer)  
    });
  })
}

/**
 * Function decode a buffer and return the json object
 * @param buffer - buffer
 */
export async function protobufDecode(buffer: number): Promise<any> {
  return new Promise( resolve => {
      var protobuf = require("protobufjs");
      protobuf.load(`${process.cwd()}/src/types/connection.proto`, function(err, root) {
      if (err)
        throw err;

      // Obtain a message type
      var ConnectionsMessage = root.lookupType("conn.ConnectionsMessage");
  
      var message = ConnectionsMessage.decode(buffer);
      
      resolve(message)
    });
  })
}
