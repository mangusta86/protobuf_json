"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
var ProgressBar = require('progress');
var WebSocket = require('ws');
const ws = new WebSocket('ws://localhost:8080');
const fs = require('fs');
const basic_1 = require("./lib/basic");
const websocket_1 = require("./lib/websocket");
const protobuf_1 = require("./lib/protobuf");
var bar;
function main() {
    let benchmark = [];
    let files = fs.readdirSync(`${process.cwd()}/data`);
    files = files.sort();
    const totalTests = files.length * 2;
    bar = new ProgressBar('Progress [:bar] :percent :etas ', { total: totalTests });
    let resCounter = 0;
    let sendCounter = 0;
    let start, end;
    ws.on('open', function open() {
        return __awaiter(this, void 0, void 0, function* () {
            console.log("Connected to WS server!");
            let data;
            for (const file of files) {
                data = fs.readFileSync(`${process.cwd()}/data/${file}`);
                let jsonData = JSON.parse(data);
                start = Date.now();
                JSON.stringify(jsonData);
                end = Date.now();
                benchmark.push({ name: file, jStart: Date.now(), jDTQ: data.length, jDSS: end - start });
                yield websocket_1.WsSendData(ws, data);
                yield basic_1.sleep(500);
                let binary = yield protobuf_1.convertJSONToBinary(JSON.parse(data));
                benchmark[sendCounter].pDTQ = Buffer.from(binary.toString()).length;
                benchmark[sendCounter].pStart = Date.now();
                yield websocket_1.WsSendData(ws, binary);
                sendCounter++;
                yield basic_1.sleep(500);
            }
        });
    });
    ws.on('message', function incoming(message) {
        bar.tick();
        let pos = Math.floor(resCounter / 2);
        if (resCounter % 2 === 0) {
            benchmark[pos].jEnd = Number(JSON.parse(message).time);
            benchmark[pos].jDTT = benchmark[pos].jEnd - benchmark[pos].jStart;
        }
        else {
            benchmark[pos].pEnd = Number(JSON.parse(message).time);
            benchmark[pos].pDTT = benchmark[pos].pEnd - benchmark[pos].pStart;
            benchmark[pos].pDSS = JSON.parse(message).DSS;
        }
        if (resCounter === totalTests - 1) {
            for (let i = 0; i < benchmark.length; i++) {
                const element = benchmark[i];
                element['jDTS'] = (Math.floor(((element['jDTQ'] / (element['jDTT'] === 0 ? 1 : element['jDTT'])) * 1000) / Math.pow(10, 6))).toString() + "MB/s";
                element['pDTS'] = (Math.floor(((element['pDTQ'] / (element['pDTT'] === 0 ? 1 : element['pDTT'])) * 1000) / Math.pow(10, 6))).toString() + "MB/s";
                delete element['jStart'];
                delete element['jEnd'];
                delete element['pStart'];
                delete element['pEnd'];
            }
            console.table(benchmark);
            process.exit();
        }
        resCounter++;
    });
}
main();
//# sourceMappingURL=index.js.map