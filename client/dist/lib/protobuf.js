"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
function convertJSONToBinary(rawConntections) {
    return __awaiter(this, void 0, void 0, function* () {
        let buffer;
        let connections = [];
        for (let i = 0; i < rawConntections.length; i++) {
            const connection = rawConntections[i];
            connections.push({
                id: connection[".id"],
                protocol: connection['protocol'],
                srcAddress: connection["src-address"],
                dstAddress: connection["dst-address"],
                replySrcAddress: connection["reply-src-address"],
                replyDstAddress: connection["reply-dst-address"],
                timeout: connection['timeout'],
                origPackets: parseInt(connection["orig-packets"]),
                origBytes: parseInt(connection["orig-bytes"]),
                origFasttrackPackets: parseInt(connection["orig-fasttrack-packets"]),
                origFasttrackBytes: parseInt(connection["orig-fasttrack-bytes"]),
                replPackets: parseInt(connection["repl-packets"]),
                replBytes: parseInt(connection["repl-bytes"]),
                replFasttrackPackets: parseInt(connection["repl-fasttrack-packets"]),
                replFasttrackBytes: parseInt(connection["repl-fasttrack-bytes"]),
                origRate: parseInt(connection["orig-rate"]),
                replRate: parseInt(connection["repl-rate"]),
                expected: Boolean(),
                seenReply: Boolean(),
                assured: Boolean(),
                confirmed: Boolean(),
                dying: Boolean(),
                fasttrack: Boolean(),
                srcnat: Boolean(),
                dstnat: Boolean()
            });
        }
        buffer = yield protobufEncode(connections);
        return (buffer);
    });
}
exports.convertJSONToBinary = convertJSONToBinary;
function protobufEncode(connections) {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise(resolve => {
            var protobuf = require("protobufjs");
            protobuf.load(`${process.cwd()}/src/types/connection.proto`, function (err, root) {
                if (err)
                    throw err;
                var ConnectionsMessage = root.lookupType("conn.ConnectionsMessage");
                var payload = { connections: connections };
                var errMsg = ConnectionsMessage.verify(payload);
                if (errMsg)
                    throw Error(errMsg);
                var message = ConnectionsMessage.create(payload);
                var buffer = ConnectionsMessage.encode(message).finish();
                resolve(buffer);
            });
        });
    });
}
function protobufDecode(buffer) {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise(resolve => {
            var protobuf = require("protobufjs");
            protobuf.load(`${process.cwd()}/src/types/connection.proto`, function (err, root) {
                if (err)
                    throw err;
                var ConnectionsMessage = root.lookupType("conn.ConnectionsMessage");
                var message = ConnectionsMessage.decode(buffer);
                resolve(message);
            });
        });
    });
}
exports.protobufDecode = protobufDecode;
//# sourceMappingURL=protobuf.js.map