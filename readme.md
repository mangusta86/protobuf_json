# ReadMe
Require Node 10.13+ for displaying benchmark results in the console.

## Installation 
```
git clone https://gitlab.com/mangusta86/protobuf_json.git
cd protobuf_json
cd client
npm install
cd ../server
npm install
```

# How to use
Start the server first and after that run the client.

For starting server
```
cd protobuf_json/server
ts-node src/index.ts
# or 
node dist/index.js
```

For starting client
```
cd protobuf_json/client
ts-node src/index.ts
# or 
node dist/index.js
```

## Report
[Read Full Report](./_report/index.md)