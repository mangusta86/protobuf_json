# Protocol Buffers vs JSON
# Protobuf
## About 
[Protocol buffers](https://en.wikipedia.org/wiki/Protocol_Buffers), also known as Protobuf, is a protocol that Google developed internally to enable serialization and deserialization of structured data between different services. Google’s design goal was to create a better method than XML to make systems communicate with each other over a wire or for the storage of data. Since its development, Google has made Protobuf under an open source license. It also provides out of the box support in the most common languages, including Python, Java, Objective-C, C# and others via Google’s new proto3 language version.
#### Pros
* compressed data (small output).
* hard to robustly decode without knowing the schema (data format is internally ambiguous, and needs a schema in order to decode).
* quick processing.
* not intended for human eyes (dense binary).
#### Cons
* dependent on a .proto compiler.

# JSON
[JSON](https://en.wikipedia.org/wiki/JSON), or JavaScript Object Notation, is a minimal, readable format also used to structure data. It is also an alternative to XML and is mainly used as a way of transmitting data between a server and web application. JSON uses human-readable way as a way of transmitting data objects made up of attribute-value pairs and array data types (or another type of serializable value). JSON was derived from JavaScript, but since last year multiple programming languages include code to generate and parse JSON-format data.
#### Pros
* human readable/editable
* can be parsed without knowing schema in advance
* excellent browser support

#### Cons
* no validation support

## Benchmark
### Test 1 (i5-5300u - 16gb ram)
| No |  name      | jDTQ      | jDTT    |  jDSS  | pDTQ     | pDTT | pDSS | jDTS    | pDTS       |
|--- |---         |---        |---      |---     |---       |---   |---   |---      |---         |
| #0 | KB032.txt  | 34074     |   4     |   1    | 10324    |   1  |   5  |  8MB/s  |   10MB/s   |
| #1 | KB156.txt  | 156562    |   3     |   1    | 51333    |   2  |   11 |  52MB/s |   25MB/s   |
| #2 | KB251.txt  | 257424    |   4     |   2    | 79955    |   2  |   15 |  36MB/s |   39MB/s   |
| #3 | MB01.txt   | 1029693   |  18     |   4    | 319820   |   5  |   26 |  57MB/s |   63MB/s   |
| #4 | MB05.txt   | 5148461   |  96     |  20    | 1599100  |  35  |   49 |  53MB/s |   45MB/s   |
| #5 | MB10.txt   | 10296921  | 162     |  39    | 3198200  |  69  |  101 |  63MB/s |   46MB/s   |

### Test 2 (i7-4550u - 8gb ram)
| No |  name      | jDTQ      | jDTT  |  jDSS  | pDTQ     | pDTT   | pDSS | jDTS   | pDTS       |
|--- |---         |---        |---    |---     |---       |---     |---   |---     |---         |
| #0 | KB032.txt  | 34074     |  3    |   1    | 10324    |   1    |  4   | 11MB/s | 10MB/s     |
| #1 | KB156.txt  | 156562    |  6    |   2    | 51333    |   1    | 10   | 12MB/s | 51MB/s     |
| #2 | KB251.txt  | 257424    |  6    |   3    | 79955    |   1    | 12   | 42MB/s | 19MB/s     |
| #3 | MB01.txt   | 1029693   | 13    |   3    | 319820   |   3    | 30   | 79MB/s | 106MB/s    |
| #4 | MB05.txt   | 5148461   | 113   |  17    | 1599100  |  12    | 38   | 45MB/s | 133MB/s    |
| #5 | MB10.txt   | 10296921  | 184   |  45    | 3198200  |  41    | 63   | 55MB/s | 78MB/s     |


Notes:<br> 
You can see labels in the benchmark table that start with **j** from JSON or **p** from Protobuf
* name of the file including the stringified JSON data
* data transmission time in ms (DTT)
* data transmission quantity in bytes(DTQ)
* data transmission speed in ms (DTS)
* data serialization speed in ms (DSS)

## Conclusion
### General
* Both protocols are mature and can be used without any limitations in all types of programming languages. 
* The implementation of both methods is straightforward with proper documentation and understanding the formats.
* The learning curve of Protobuf if higher compared with JSON, but not too steep.
* JSON is appropriate for human readable data

### Performance
* The data size quantity of Protobuf is with ~70% lower compared to JSON.
* Protobuf is with 25%-300% faster in transmission speed. 
* In NodeJS the serialization is slower compared with JSON deserialization.
* On small packages, the DTT is irrelevant because it depends on the processor threads management.
* Performance depends on the hardware and NodeJS as well. 100% transmission speed improvement on the last version Node (12.3.1) was observed.

### Programming Language Support 
* The following frameworks: protobufjs, google-protobuf, pbf, can be used in NodeJS environment. Protobufjs is the fastest.
* Protobuf compiler generates *.pb.h and *.pb.cc files in C/C++ enviroment.
* The serialization speed in C/C++ is higher than in NodeJS and better compared with JSON serialization.

### Possible future concerns
* Must have a well-structured data if Protobuf is used.
* Although Protobuf has many advantages, such as speed & size of its performance, it has a minor community