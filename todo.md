# Node.JS Test for IoT
	
Make a comparison benchmark test between different data serialization methods over WebSocket communication protocol.

## Description
You will have to make a benchmark test exposing the advantages/disadvantages of Protocol Buffers (Protobuf, in particular) and JSON data serialization methods.

## Requirements
1. Write a short description of both methods exposing the theoretical pros/cons.
2. Build running client/server application(s) that will be capable of exchanging data in both JSON and Protobuf methods.
3. Create a benchmark test between the two methods, making different types of tests that will show the theoretical differences exposing these KPIs, but not limited to: 
    * data transmission quantity (DTQ)
    * data transmission speed (DTS)
    * data serialization speed (DSS)

4. Write a short conclusion analyzing the following, but not limited to:
    * Learning curve
    * Ease of implementation
    * LTS (long term stability) of the protocol
    * Plugin/frameworks availability on these programming languages: C/C++, Node.JS
    * Possible future concerns.
## Technological stack
Use node.js for client/server application.

## Recommendations
* The test is not solely about node.js implementation, but about the overall architecture. 
* There is no time limit, you will be solely responsible for estimating the effort.
* You will be responsible for creating the tests for various scenarios, not limited to the following examples (whatever you think is necessary):
    * small/big data frames
    * small/big data frames transmitted
    * CPU/memory consumption
* We will assume that everything is correct, so try not to implement validation or much error checking logic.
* Make everything light and try to not use to much overhead.

## Results
* You can send the deliverables in any format you prefer (ZIP attachments, GitHUB repository etc.)
* If there are certain recommendations, please provide a README file.